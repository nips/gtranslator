#ifndef WINDOW_H
#define WINDOW_H

#include <QDialog>
#include <QtGui>
#include <QtNetwork/QNetworkReply>

class Window : public QDialog
{
    Q_OBJECT

public:
    Window();

private slots:
    void toggleWindow();
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void translate();
    void serviceRequestFinished(QNetworkReply* reply);
    void readGcloudCommandOutput();
    void clearTexts();

private:
    QPushButton *okButton;
    QPushButton *cancelButton;
    QPushButton *clearButton;
    QTextEdit *sourceText;
    QTextEdit *resultText;
    QLabel *sourceLabel;
    QLabel *resultLabel;
    QComboBox *sourceLang;
    QComboBox *resultLang;
    QString apiKey;
    QProcess *process;
    QString languageCodes[2];
    int counter;

    QAction *quitAction;

    QSystemTrayIcon *trayIcon;

    QMenu *trayIconMenu;

    QVBoxLayout *appLayout;
    QHBoxLayout *mainLayout;
    QVBoxLayout *textsLayout;
    QVBoxLayout *optionsLayout;
    QHBoxLayout *mainButtonsLayout;

    void executeGcloudCommand();
    void createMenuActions();
    void createTrayIcon();
    void alert(QString message);
};

#endif // WINDOW_H
