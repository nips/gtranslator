#include <QtGui>
#include <iostream>
#include "window.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::critical(0, QObject::tr("GTranslator"),
                              QObject::tr("Nie można wykryć tacki systemowej "));
        return 1;
    }
    // QApplication::setQuitOnLastWindowClosed(false);

    Window window;
    window.show();

    return a.exec();
}
