#include <iostream>
#include <QtGui>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QByteArray>
#include <QVariantMap>
#include <qjson/parser.h>
#include "window.h"

Window::Window()
{
    counter = 0;
    executeGcloudCommand();
    languageCodes[0] = QString("pl");
    languageCodes[1] = QString("en");

    QRect geometry = QApplication::desktop()->screenGeometry(QApplication::desktop()->primaryScreen());
    int maxX = geometry.width();
    int maxY = geometry.height();

    createMenuActions();
    createTrayIcon();

    clearButton = new QPushButton(tr("Clear"));
    okButton = new QPushButton(tr("Translate"));
    okButton->setDefault(true);
    cancelButton = new QPushButton(tr("Hide"));

    sourceText = new QTextEdit();
    resultText = new QTextEdit();

    QLabel *sourceLabel = new QLabel(tr("Source text"));
    QLabel *resultLabel = new QLabel(tr("Result"));

    appLayout = new QVBoxLayout();
    mainLayout = new QHBoxLayout();
    textsLayout = new QVBoxLayout();
    optionsLayout = new QVBoxLayout();
    mainButtonsLayout = new QHBoxLayout();

    connect(clearButton, SIGNAL(clicked()), this, SLOT(clearTexts()));
    connect(okButton, SIGNAL(clicked()), this, SLOT(translate()));
    connect(cancelButton, SIGNAL(clicked()), this, SLOT(toggleWindow()));
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
    //connect(process, SIGNAL(readyReadStandardOutput()),
    //        this, SLOT(readGcloudCommandOutput()));

    setLayout(appLayout);

    // ok, cancel
    mainButtonsLayout->addStretch(1);
    mainButtonsLayout->addWidget(cancelButton, 0, Qt::AlignRight);
    mainButtonsLayout->addWidget(clearButton, 0, Qt::AlignRight);

    // texts
    textsLayout->addWidget(sourceLabel);
    textsLayout->addWidget(sourceText);
    textsLayout->addWidget(resultLabel);
    textsLayout->addWidget(resultText);

    // comboboxes
    sourceLang = new QComboBox();
    sourceLang->addItem(tr("Polski"), QVariant(0));
    sourceLang->addItem(tr("Angielski"),  QVariant(1));
    sourceLang->setCurrentIndex(1);
    resultLang = new QComboBox();
    resultLang->addItem(tr("Polski"), QVariant(0));
    resultLang->addItem(tr("Angielski"), QVariant(1));
    resultLang->setCurrentIndex(0);

    optionsLayout->addWidget(okButton);
    optionsLayout->addWidget(sourceLang);
    optionsLayout->addWidget(resultLang);
    appLayout->addStretch(3);

    mainLayout->addLayout(textsLayout);
    mainLayout->addLayout(optionsLayout);

    appLayout->addLayout(mainLayout);
    appLayout->addStretch(1);
    appLayout->addLayout(mainButtonsLayout);

    trayIcon->setIcon(QIcon(":/images/gt1.png"));
    trayIcon->show();

    setWindowTitle(tr("GTranslator"));
    resize(600, 300);
    move(maxX-600, maxY-345);
    show();
}

void Window::createMenuActions()
{
    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void Window::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(quitAction);
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
}

void Window::toggleWindow()
{
    if (isVisible()) {
        hide();
    } else {
        show();
    }
}

void Window::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
        toggleWindow();
        break;
    case QSystemTrayIcon::MiddleClick:
        //showMessage();
        break;
    default:
        ;
    }
}

void Window::translate()
{
    // Setup the webservice url
    QUrl serviceUrl = QUrl("https://translation.googleapis.com/language/translate/v2");

    QByteArray postData;
    QString pdata = "{\"q\":\"" + sourceText->toPlainText() + "\",\"source\":\"" + languageCodes[sourceLang->currentIndex()] + "\",\"target\":\"" + languageCodes[resultLang->currentIndex()] + "\",\"format\":\"text\"}";
    postData.append(pdata.toAscii());
    //qDebug() << pdata;

    // call the webservice
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QString headerData = "Bearer " + apiKey;
    QNetworkRequest request(serviceUrl);
    request.setRawHeader("Authorization", headerData.toLatin1());//  Local8Bit());
    request.setRawHeader("Content-Type", "application/json");

    connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(serviceRequestFinished(QNetworkReply*)));
    manager->post(request, postData);
}

void Window::serviceRequestFinished(QNetworkReply* reply)
{
    QByteArray buffer = reply->readAll();
    //qDebug() << buffer;
    QJson::Parser parser;
    bool ok;

    QVariantMap result = parser.parse(buffer, &ok).toMap();
    if (!ok)
    {
        alert("An error occurred during parsing");
    }
    if (result.contains("error"))
    {
        if (counter<2)
        {
            counter++;
            executeGcloudCommand();
            translate();
        } else {
            alert(result["error"].toMap()["errors"].toList()[0].toMap()["message"].toString());
        }
        return;
    }

    counter = 0;

    QString s = result["data"].toMap()["translations"].toList()[0].toMap()["translatedText"].toString();

    resultText->setText(s);
    // qDebug() << "response:" << s;
}

void Window::executeGcloudCommand()
{
    process = new QProcess();
    process->start("gcloud auth print-access-token");
    process->waitForFinished(-1);
    readGcloudCommandOutput();
}

void Window::readGcloudCommandOutput()
{
    apiKey = process->readAllStandardOutput();
    apiKey.remove(QRegExp("[\\n\\t\\r]"));
    if (apiKey.length()==0)
    {
        alert(tr("I can't call the gcloud command"));
    }
    //qDebug() << apiKey;
}

void Window::alert(QString message)
{
    QMessageBox *msgBox = new QMessageBox(this);
    //msgBox->setWindowModality(Qt::NonModal);
    msgBox->setInformativeText(message);
    msgBox->setStandardButtons(QMessageBox::Ok);
    msgBox->setDefaultButton(QMessageBox::Ok);
    //msgBox->setBaseSize(QSize(800, 120));
    msgBox->exec();
    exit(1);
}

void Window::clearTexts()
{
    sourceText->setText("");
    resultText->setText("");
}
